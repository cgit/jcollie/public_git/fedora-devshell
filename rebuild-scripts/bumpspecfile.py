#!/usr/bin/python -t
# -*- mode: Python; indent-tabs-mode: nil; -*-

import fcntl
import os, sys
import rpmUtils
import shutil
import string
import re
import time

EXIT_ON_WARN = False

class SpecFile:
    def __init__(self,filename):
        self.filename = filename
        file=open(filename,"r")
        self.lines=file.readlines()
        file.close()

    def bumpRelease(self):
        bump_patterns=[(re.compile(r"^Release:(\s*)(\d+.*)",re.I), self.increase),
                       (re.compile(r"^%define\s+rel\s+(\d+.*)"), self.increase2),
                       (re.compile(r"^%define\s+release\s+(\d+.*)"), self.increase3),
                       (re.compile(r"^%define\s+RELEASE\s+(\d+.*)"), self.increase4),
                       (re.compile(r"^Release:\s+%release_func\s+(\d+.*)"), self.increase5)
                       ]
        skip_pattern=re.compile(r"\$Revision:")
        for i in range(len(self.lines)):
            if skip_pattern.search(self.lines[i]): continue
            for bumpit, bumpit_func in bump_patterns:
                self.lines[i]=bumpit.sub(bumpit_func,self.lines[i])

    def addChangelogEntry(self,entry,email):
        versionre=re.compile(r"^Version:\s*(\S+)")
        changematch=re.compile(r"^%changelog")
        date=time.strftime("%a %b %d %Y",   time.localtime(time.time()))
        for i in range(len(self.lines)):
            versionmatch=versionre.search(self.lines[i])
            if(versionmatch):
                version=versionmatch.group(1)
            if(changematch.match(self.lines[i])):
                newchangelogentry="%changelog\n* "+date+" "+email+" "+version+"-"+self.newrelease+"\n"+entry+"\n\n"
                self.lines[i]=newchangelogentry
                break

    def increaseMain(self,release):
        relre = re.compile(r'(?P<pre>0\.)?(?P<rel>\d+)(?P<post>.*)',re.I)
        relmatch = relre.search(release)

        pre = relmatch.group('pre')
        value = int(relmatch.group('rel'))
        self.newrelease = `value+1`
        post = relmatch.group('post')

        old = ''
        if pre != None:
            old += pre
        old += relmatch.group('rel')+post

        if pre == None:
            if post.find('rc')>=0:
                print 'CRUCIAL WARNING: Bad pre-release versioning scheme!'
                print self.filename
                if EXIT_ON_WARN:
                    sys.exit(1)
            new = `value+1`+post
            if True or post != '%{?dist}' and len(post):
                self.debugdiff(old,new)
        else:
            if value == None or value > 10000:
                print 'CRUCIAL WARNING: Bad pre-release versioning scheme!'
                print self.filename
                if EXIT_ON_WARN:
                    sys.exit(1)
            new = '0.'+`value+1`+post
            self.debugdiff(old,new)
        return new

    def increase(self,match):
        return 'Release:' + match.group(1) + self.increaseMain(match.group(2))

    def increase2(self,match):
        return '%define rel ' + self.increaseMain(match.group(1))

    def increase3(self,match):
        return '%define release ' + self.increaseMain(match.group(1))

    def increase4(self,match):
        return '%define RELEASE ' + self.increaseMain(match.group(1))

    def increase5(self,match):
        return 'Release: %release_func ' + self.increaseMain(match.group(1))

    def writeFile(self,filename):
        file=open(filename,"w")
        file.writelines(self.lines)
        file.close()

    def debugdiff(self,old,new):
        print '-%s' % old
        print '+%s\n' % new

if __name__=="__main__":
    if len(sys.argv) < 2:
        print 'SYNTAX: %s <specfile> [specfile]...' % sys.argv[0]
        sys.exit(22)

    userstring = "You <Your.Address@your.domain>"
    for aspec in sys.argv[1:]:
        s=SpecFile(aspec)
        s.bumpRelease()
        s.addChangelogEntry(" - rebuilt for unwind info generation, broken in gcc-4.1.1-21", userstring)
        s.writeFile(aspec)

sys.exit(0)
