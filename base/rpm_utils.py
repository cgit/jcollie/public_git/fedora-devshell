# Fedora Developer Shell
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors: Yaakov M. Nemoy <ynemoy@redhat.com>
#
from __future__ import with_statement

import re
import rpm

from contextlib import contextmanager
from base import log

class RPMSpec(object):
    def __init__(self, spec_file):
        self.contents = [x for x in file(spec_file)]
    
    def version(self):
        ver_line = [x for x in self.contents if x.startswith('Version:')][0]
        ver_re = re.search(r'Version: (.*)', ver_line)
        return ver_re.groups()[0]
    
    def rel(self):
        rel_line = [x for x in self.contents if x.startswith('Release:')][0]
        rel_re = re.search(r'Release: (.*)', rel_line)
        return rel_re.groups()[0] 

@contextmanager
def rpm_macros(**keys):
    for key, value in keys.iteritems():
        log.debug('setting...')
        log.debug(key + ' ' + value)
        rpm.addMacro(key, value)
    yield
    for key, value in keys.iteritems():
        rpm.delMacro(key)

__all__ = ['rpm_macros', 'RPMSpec']
